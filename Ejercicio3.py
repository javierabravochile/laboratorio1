#!/usr/bin/env python
# -*- coding: utf-8 -*-

#se define una lista libre parra rellenar
notas = []

#funcion donde si se cumple la caracteristica se le agrega a la lista
while (True):

    nota = input('Ingrese la nota del alumno: ')
        notas.append(nota)

    else:
        break
promedio = sum(notas) / float(len(notas))


print "El promedio de las %d notas ingresadas es %.1f" % (len(notas), promedio)

#se retorna el promedio de notas 
 
 #funcion que va juntando las otas para luego promediarlas
def promediarnotas(notas):
    sum=0.0
    for i in range(0,len(notas)):
        sum=sum+notas[i]
 
    return sum/len(notas)
 
#funcion que nos mostrara las notas que fueron ingresadas
def imprimirnotas(notas,nombre):
    for i in range(0,len(notas)):
        print nombre + "[" + str(i) + "]=" + str(notas[i])
 
#funcion que contendra en una tupla la lista
def leernotas():
    notas=[]
    
#se condiciona el numero de estudiantes que son para luego ir añadiendoles notas
    i=0
    while i < 30:
        notas.append(int(random.randint(0, 30)))
        i=i+1
    return notas
 
#variable que contendra la lista de notas y luego las imprimira
A=leernotas()
imprimirnotas(A,"A")
print "Promedio = " + str(promediarnotas(A))

except ValueError:
    print("El valor es incorrecto")
